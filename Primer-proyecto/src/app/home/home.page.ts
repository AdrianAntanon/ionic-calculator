import { Component } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  display = '0';
  firstValue: number = null;
  prevValue: number = 0;
  operator: any = null;
  newCursor = false;
  isClear = false;
  isComma = false;

  constructor(private alertCtrl: AlertController) {}

  async presentAlert() {
    let messageHeader = 'Operación de ';
    switch (this.operator) {
      case '+':
        messageHeader += 'suma';
        break;
      case '-':
        messageHeader += 'resta';
        break;
      case '/':
        messageHeader += 'divisón';
        break;
      case '*':
        messageHeader += 'multiplicación';
        break;
    }

    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: messageHeader,
      subHeader: `Valor previo: ${this.prevValue}`,
      message: `Resultado obtenido: ${this.display}`,
      buttons: ['Confirmar'],
    });
    await alert.present();
  }

  calculate() {
    this.prevValue = this.firstValue;
    switch (this.operator) {
      case '+':
        if (this.isComma) {
          this.firstValue = this.firstValue + parseFloat(this.display);
        } else {
          this.firstValue = this.firstValue + parseInt(this.display, 0);
        }
        break;
      case '-':
        if (this.isComma) {
          this.firstValue = this.firstValue - parseFloat(this.display);
        } else {
          this.firstValue = this.firstValue - parseInt(this.display, 0);
        }
        break;
      case '/':
        if (this.isComma) {
          this.firstValue = this.firstValue / parseFloat(this.display);
        } else {
          this.firstValue = this.firstValue / parseInt(this.display, 0);
        }
        break;
      case '*':
        if (this.isComma) {
          this.firstValue = this.firstValue * parseFloat(this.display);
        } else {
          this.firstValue = this.firstValue * parseInt(this.display, 0);
        }
        break;
    }
    this.display = this.firstValue.toString();
    this.presentAlert();
  }

  addNumberOrSimbol(value: number | string) {
    switch (value) {
      case 'ac':
        this.display = '0';
        this.firstValue = null;
        this.operator = null;
        this.newCursor = false;
        break;
      case '+':
        this.addOperator('+');
        break;
      case '-':
        this.addOperator('-');
        break;
      case '/':
        this.addOperator('/');
        break;
      case '*':
        this.addOperator('*');
        break;
      case ',':
        this.addComma();
        break;
      case '=':
        if (this.firstValue !== null && this.operator !== null) {
          this.calculate();
        }
        this.operator = null;
        break;
      default:
        const valueToString = value.toString();
        const valueToNumber: number = parseInt(valueToString);
        this.addNumber(valueToNumber);
    }
  }

  addComma() {
    if (!this.isComma) {
      this.isComma = true;
    } else {
      this.isComma = false;
    }
  }

  addNumber(nbr: number) {
    if (nbr === 0) {
      if (this.newCursor) {
        this.display = nbr.toString();
        this.newCursor = false;
      } else if (this.display !== '0') {
        if (this.isComma) {
          this.display = `${this.display.toString()}.${nbr}`;
        } else {
          this.display = this.display.toString() + nbr;
        }
      } else {
        if (this.isComma) {
          this.display = `${this.display.toString()}.${nbr}`;
        }
      }
    } else {
      if (this.newCursor) {
        this.display = nbr.toString();
        this.newCursor = false;
      } else if (this.display === '0') {
        if (this.isComma) {
          if (this.display.toString().indexOf('.') > -1) {
            this.display = this.display.toString() + nbr;
          } else {
            this.display = `${this.display.toString()}.${nbr}`;
          }
        } else {
          this.display = nbr.toString();
        }
      } else {
        if (this.isComma) {
          if (this.display.toString().indexOf('.') > -1) {
            this.display = this.display.toString() + nbr;
          } else {
            this.display = `${this.display.toString()}.${nbr}`;
          }
        } else {
          this.display = this.display.toString() + nbr;
        }
      }
    }

    this.isClear = true;
  }

  addOperator(oprtr: string) {
    if (!this.newCursor) {
      if (this.firstValue === null) {
        if (this.isComma) {
          this.firstValue = parseFloat(this.display);
        } else {
          this.firstValue = parseInt(this.display, 0);
        }
      }
      if (this.firstValue !== null && this.operator !== null) {
        this.calculate();
      }
    }
    this.isComma = false;
    this.operator = oprtr;
    this.newCursor = true;
  }
}
